
DogSafari =

  csrftoken: ''

  root: '/dogsafari'

  setupCsrf: ->
    @csrftoken = @getCookie("csrftoken")

    $.ajaxSetup beforeSend: (xhr, settings) =>
      # Send the token to same-origin, relative URLs only.
      # Send the token only if the method warrants CSRF protection
      # Using the CSRFToken value acquired earlier
      xhr.setRequestHeader "X-CSRFToken", @csrftoken  if not @csrfSafeMethod(settings.type) and @sameOrigin(settings.url)
      return

  # using jQuery
  getCookie: (name) ->
    cookieValue = null
    if document.cookie and document.cookie isnt ""
      cookies = document.cookie.split(";")
      i = 0

      while i < cookies.length
        cookie = jQuery.trim(cookies[i])
        
        # Does this cookie string begin with the name we want?
        if cookie.substring(0, name.length + 1) is (name + "=")
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
          break
        i++
    cookieValue

  csrfSafeMethod: (method) ->
    # these HTTP methods do not require CSRF protection
    /^(GET|HEAD|OPTIONS|TRACE)$/.test method

  sameOrigin: (url) ->
    # test that a given url is a same-origin URL
    # url could be relative or scheme relative or absolute
    host = document.location.host # host + port
    protocol = document.location.protocol
    sr_origin = "//" + host
    origin = protocol + sr_origin
    
    # Allow absolute or scheme relative URLs to same origin
    
    # or any other URL that isn't scheme relative or absolute i.e relative.
    (url is origin or url.slice(0, origin.length + 1) is origin + "/") or (url is sr_origin or url.slice(0, sr_origin.length + 1) is sr_origin + "/") or not (/^(\/\/|http:|https:).*/.test(url))


  now: ->
    moment()

  clearAlerts: ->
    $('#alerts').empty()

  addAlert: (alert) ->
    # add the new one
    $('#alerts').append(alert.message)

  # Keep active application instances namespaced under an app object.
  app: _.extend({}, Backbone.Events)

module.exports = DogSafari


