
DogSafari = require "./dogsafari.coffee"
Views = require "./views.coffee"

Router = Backbone.Router.extend

  initialize: ->
    Backbone.history.start
      pushState: true
      root: DogSafari.root

  views: {}
  routes:
    '(/)': 'main'
    'add(/)': 'add'
    'search/(:query)': 'main'

  main: (query) ->
    console.log query
    @views.main = Views.Overview({searchTerm:query})
    React.renderComponent(@views.main, document.getElementById('main'))

  add: ->
    @views.add = Views.Add()
    React.renderComponent(@views.add, document.getElementById('main'))


#set up the csrf token
DogSafari.setupCsrf()

# Start up our app
DogSafari.app.router = new Router()

# All navigation that is relative should be passed through the navigate
# method, to be processed by the router.  If the link has a data-bypass
# attribute, bypass the delegation completely.
$('body').on "click", "a:not([data-bypass], .chzn-default, .chzn-single)", (evt) ->
  href = $(this).attr("href")

  # Ensure the protocol is not part of URL, meaning its relative.
  if href
    console.log href
    evt.preventDefault()
    DogSafari.app.router.navigate href, {trigger: true}

