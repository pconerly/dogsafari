###*
  @jsx React.DOM
###

DogSafari = require "./dogsafari.coffee"


Add = require "./views/add.coffee"
Overview = require "./views/overview.coffee"

Views = {}

Views.Add = Add
Views.Overview = Overview

module.exports = Views
