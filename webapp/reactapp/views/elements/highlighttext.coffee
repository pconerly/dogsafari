DogSafari = require '../../dogsafari.coffee'

{
  a,
  div, 
  h2,
  i
  img,
  p,
  span,
} = React.DOM


HighlightText = React.createClass

  render: ->
    splitText = []
    if @props.searchTerm and @props.searchTerm != ''
      lowerText = @props.children.toLowerCase()
      searchTermIndex = lowerText.indexOf @props.searchTerm
      if searchTermIndex != -1
        splitText[0] = @props.children[...searchTermIndex]
        splitText[1] = @props.children[searchTermIndex...searchTermIndex + @props.searchTerm.length]
        splitText[2] = @props.children[searchTermIndex + @props.searchTerm.length...]

    if splitText.length > 0
      return span {}, [
        splitText[0]
        span {className:'highlight'}, splitText[1]
        splitText[2]
      ]
    else
      return span {}, @props.children


module.exports = HighlightText
