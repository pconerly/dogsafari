DogSafari = require '../../dogsafari.coffee'

{
  a, 
  button, 
  div, 
  input,
  label,
  p
} = React.DOM


FormItem = React.createClass
  # do i need to get a csrf token?
  getInitialState: ->
    return {
      value: ''
      error: null
    }

  onChange: (e) ->
    @setState {
      value: e.target.value
    }

  getHelpText: ->
    if @props.helpText
      return p {className:"help-block"}, @props.helpText
    else
      return ''

  getErrorText: ->
    if @state.error
      return p {className:"help-block"}, @state.error.toString()
    else
      return ''

  addError: (error) ->
    @setState
      error: error

  removeError: ->
    @setState 
      error: null

  render: ->
    return (div {className:"form-group #{if @state.error then 'has-error' else ''}"},
      [
        label {className:"control-label"}, @props.children
        input {
          id: "#{@props.key}FormItem"
          ref: "#{@props.key}FormItem"
          type:"#{@props.type}", 
          className:"form-control"
          onChange: @onChange
          value: @state.value }
        @getHelpText()
        @getErrorText()
      ]
    )

module.exports = FormItem
