DogSafari = require '../../dogsafari.coffee'

{
  a, 
  button,
  div, 
  form,
  input,
  nav,
  span
} = React.DOM


Navbar = React.createClass

  getInitialState: ->
    return {
      searchtext: ''
    }

  onChange: (e) ->
    @setState {
      searchtext: e.target.value
    }

  handleSubmit: (e) ->
    e.preventDefault()
    DogSafari.app.router.navigate "/search/#{@state.searchtext}", {trigger: true}

  render: -> 
    return @transferPropsTo (div {},
      nav {
        className: 'navbar navbar-default navbar-inverse navbar-fixed-top'
        role: 'navigation'
        },
        div {className: 'navbar-header'},
          a {
            className: 'navbar-brand'
            href: "#{DogSafari.root}"
            'data-bypass': true
            }, "Dog Safari"
        form {
          id:"dogsearch"
          className:"navbar-form navbar-right" 
          role:"search"
          onSubmit: @handleSubmit
          }, [
          div {className:"input-group input-group-sm navbar-input"}, [
            input {
              type:'text',
              name:'search'
              className:'form-control', 
              placeholder:'Search',    
              onChange: @onChange,
              value: @state.searchtext,
            }
            span {className:"input-group-btn"},
              button {className:"btn btn-default", type:"submit"}, 'Search'
          ]
        ]

      div {className:"container"}, @props.children
      )

module.exports = Navbar
