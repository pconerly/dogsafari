DogSafari = require '../../dogsafari.coffee'

FormItem = require './formitem.coffee'

{
  a, 
  button, 
  div, 
  form,
  i,
  span,
} = React.DOM


DogForm = React.createClass

  getInitialState: ->
    return {
      loading: false
    }

  handleSubmit: (e) ->
    e.preventDefault()
    @setState {
      loading: true
    }
    formData = new FormData()
    file = $('#imgFormItem')[0].files[0]
    formData.append('file', file) 

    for child in @props.children
      child.removeError()
      formData.append(child.props.key, child.state.value)

    $.ajax
      url: @props.action 
      type: 'POST'
      data: formData
      #Options to tell JQuery not to process data or worry about content-type
      cache: false
      contentType: false
      processData: false
      success: (data) =>
        @setState {
          loading: false
        }
        if data.status == 'success'
          # I may also need to clear the form
          DogSafari.app.router.navigate '/', {trigger: true}
        else if data.errors
          @showErrors data.errors
        else
          DogSafari.addAlert
            message: "There was an error submitting the form."

      error: (xhr, status, errorThrown) =>
        @setState {
          loading: false
        }
        DogSafari.addAlert
          context: 'error'
          message: """
            AJAX Error: #{status}<br/>
            AJAX error thrown: #{errorThrown}<br />
            AJAX XHR: #{xhr}<br />
            """

  showErrors: (formErrors) ->
    childrenLookup = {}
    for child in @props.children
      childrenLookup[child.props.key] = child

    for key, error of formErrors
      childrenLookup[key].addError error

  render: ->
    return (form {
      onSubmit: @handleSubmit
      role:'form',},
      @props.children
      button {
        type:"submit",
        className:"has-spinner btn btn-default #{if @state.loading then 'active disabled'}"
      }, [
        span {className:'spinner'}, i {className:'icon-spin icon-refresh'}
        "Submit" 
      ]
    )


module.exports = DogForm
