DogSafari = require '../../dogsafari.coffee'

HighlightText = require './highlighttext.coffee'

{
  a,
  div, 
  h2,
  i
  img,
  p,
  span,
} = React.DOM


DogPanel = React.createClass

  getInitialState: ->
    return {
      lightbox: false
    }

  toggleLightbox: (e) ->
    e.preventDefault()
    @setState {
      lightbox: !@state.lightbox
    }

  getLightbox: ->
    return div {}, [
      div {className:"black-overlay"}
      div {className:"white-content well"}, [
        a {
          className:"btn btn-small btn-warning pull-right"
          onClick: @toggleLightbox
        }, [
          i {className:"glyphicon glyphicon-remove"}
          "Close"
        ]
        h2 {className:'lightbox-title'}, @props.item.name
        span {className:"lightbox-subtitle"}, "owned by #{@props.item.owner}"
        img {
          className:"img-full"
          src:"/media/dogpics/#{@props.item.img}"
        }
      ]
    ]

  render: ->
    if @state.lightbox
      return @getLightbox()
    else
      return div {
        className: 'panel panel-default panel-thumb'
        onClick: @toggleLightbox
      }, div {className:'panel-body'}, [
          img {
            className:"img-thumb"
            src:"/media/dogpics/#{@props.item.thumbnail}"
          }
          div {className:'panel-text'}, [
            p {className:"panel-title"}, (HighlightText {searchTerm:@props.searchTerm}, @props.item.name)
            p {className:"pull-right panel-subtitle"}, [
              "owned by "
              HighlightText {searchTerm:@props.searchTerm}, @props.item.owner
            ]
          ]
        ]


module.exports = DogPanel
