DogSafari = require '../dogsafari.coffee'

DogForm = require './elements/dogform.coffee'
FormItem = require './elements/formitem.coffee'
Navbar = require './elements/navbar.coffee'

{
  a,
  button,
  div,
  h4, 
  hr,
  legend,
  li,
  p,
  ul
} = React.DOM

Add = React.createClass

  render: ->
    return @transferPropsTo (
      Navbar {}, [
        a {
          className:"btn btn-small btn-info pull-right disabled"
          href:"add"
        }, "Add a dog"
        a {
          className:"btn btn-small btn-info pull-right push-right20"
          href:"/"
        }, "All the dogs"
        h4 {}, "Add a dog!"
        div {className:"clear"}, (
          DogForm {
            action: "#{DogSafari.root}/api/adddog/"
            }, [
              FormItem {key:'name', type:"text"}, "Dog Name"
              FormItem {key:'owner', type:"text"}, "Owner's name"
              FormItem {
                key:'img',
                type:"file",
                helpText: "Upload an image of your dog from your computer."
              }, "Picture of the dog!"
            ]
        )
      ]
    )

module.exports = Add





