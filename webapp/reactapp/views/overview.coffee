DogSafari = require '../dogsafari.coffee'

Navbar = require './elements/navbar.coffee'
DogForm = require './elements/dogform.coffee'
DogPanel = require './elements/dogpanel.coffee'
FormItem = require './elements/formitem.coffee'
HighlightText = require './elements/highlighttext.coffee'

{
  a,
  button,
  div,
  h4, 
  hr,
  img,
  legend,
  li,
  p,
  ul
} = React.DOM

Overview = React.createClass
  getInitialState: ->
    @getDogsOrSearch @props
    return {
      dogs: []
    }

  componentWillReceiveProps: (nextProps) ->
    DogSafari.clearAlerts()
    @getDogsOrSearch nextProps

  getDogsOrSearch: (props) ->
    if props.searchTerm and props.searchTerm != ''
      @getSearch props.searchTerm
      if props.searchTerm.length < 3
        DogSafari.addAlert
          message: "Search terms must be at least three characters."
    else
      @getDogs()

  getDogs: ->
    $.ajax "#{DogSafari.root}/api/dogs/",
      type: 'get'
      dataType: 'json'
      contentType: 'text/json'
      error: (xhr, status, errorThrown) =>
        DogSafari.addAlert
          context: 'error'
          message: """
            AJAX Error: #{status}<br/>
            AJAX error thrown: #{errorThrown}<br />
            AJAX XHR: #{xhr}<br />
            """
      success: (data, status, xhr) =>
        @setState {
          dogs: if _.has(data, 'length') then data else []
        }

  getSearch: (searchTerm) ->
    data = {
      search: searchTerm
    }

    $.ajax "#{DogSafari.root}/api/search/",
      type: 'POST'
      dataType: 'json'
      contentType: 'text/json'
      data: JSON.stringify(data)
      # data: data
      error: (xhr, status, errorThrown) =>
        DogSafari.addAlert
          context: 'error'
          message: """
            AJAX Error: #{status}<br/>
            AJAX error thrown: #{errorThrown}<br />
            AJAX XHR: #{xhr}<br />
            """
      success: (data, status, xhr) =>
        if data.status == 'success'
          @setState {
            dogs: if _.has(data.dogs, 'length') then data.dogs else []
          }
        else
          DogSafari.addAlert
            message: "There was an error searching."

  createItem: (item) ->
    return DogPanel {
      item:item
      searchTerm: @props.searchTerm
    }

  getTitleText: ->
    if @props.searchTerm and @props.searchTerm != ''
      return [
        "Search results for \""
        HighlightText {searchTerm: @props.searchTerm}, "#{@props.searchTerm}\":"
      ]
    else
      return "All the dogs"

  render: ->
    return @transferPropsTo (
      Navbar {}, [
        a {
          className:"btn btn-small btn-info pull-right"
          href:"add"
        }, "Add a dog"
        if @props.searchTerm and @props.searchTerm != '' then a {
          className:"btn btn-small btn-info pull-right push-right20"
          href:"/"
        }, "All the dogs"

        h4 {className:'title-container'}, @getTitleText()
        div {className:"clear"}, [
          if @state.dogs.length is 0 then h4 {}, "No results found. Have you added any dogs?" else null
          (div {}, [
            @state.dogs.map @createItem
          ]),
          (hr {className: 'clear tall70'})
        ]
      ])

module.exports = Overview

