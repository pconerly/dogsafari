import datetime
from haystack import indexes
from dogsafari.models import Dog


class DogIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True, 
        template_name='search_dog.txt')
    name = indexes.CharField(model_attr='name')
    owner = indexes.CharField(model_attr='owner')

    def get_model(self):
        return Dog
