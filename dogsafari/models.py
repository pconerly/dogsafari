import os, sys
from PIL import Image

from django.db import models
from django.template.defaultfilters import slugify
from rest_framework import serializers, viewsets
from rest_framework.response import Response

from rover_interview import settings


size = 150, 150

class Dog(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=192, default="")
    owner = models.CharField(max_length=192, default="")
    img = models.FileField(blank=True, upload_to='dogpics')
    thumbnail = models.FileField(blank=True, upload_to='dogpics')
    slug = models.SlugField(blank=True, null=True, default=None)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)[:50]
        super(Dog, self).save(*args, **kwargs)


    def processThumbnail(self):
        try:
            print os.path.join(settings.DOG_MEDIA_ROOT, self.img.name)
            im = Image.open(os.path.join(settings.DOG_MEDIA_ROOT, self.img.name))
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(os.path.join(settings.DOG_MEDIA_ROOT, "thumbnails", self.img.name), "PNG")

            self.thumbnail = os.path.join("thumbnails", self.img.name)
            self.save()
        except IOError, e:
            print "cannot create thumbnail for '%s'" % self.img
            print e

class DogSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Dog
        fields = ('id', 'url', 'name', 'owner','thumbnail', 
            'img', 'slug')

class DogViewSet(viewsets.ModelViewSet):
    model = Dog
    serializer_class = DogSerializer
