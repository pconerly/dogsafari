from django.conf.urls import url, patterns, include
from rest_framework import routers

from dogsafari.models import DogViewSet


router = routers.DefaultRouter()
router.register(r'dogs', DogViewSet)

urlpatterns = patterns('',
    url(r'^api/adddog', 'dogsafari.views.addDog'), # custom url for the dog form
    url(r'^api/search', 'dogsafari.views.search'), # custom url for searching dogs
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'', 'dogsafari.views.index'), # server the static react file
)
