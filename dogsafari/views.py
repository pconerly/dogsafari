import json
import logging
import os
import sys

from django.shortcuts import render
from django.shortcuts import (  render_to_response, 
                                get_object_or_404, 
                                HttpResponse,
                                )
from django.template import RequestContext
from haystack.query import SearchQuerySet

from dogsafari.forms import DogForm
from dogsafari.models import Dog, DogSerializer
from dogsafari.search_indexes import DogIndex
from rover_interview import settings


def index(request):
    return render_to_response('base.html', 
                                locals(), 
                                context_instance=RequestContext(request))

def addDog(request):
    if request.method == 'POST': # If the form has been submitted...
        form = DogForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            newDog = form.save()
            return addPic(request, newDog.id)
        else:
            return HttpResponse(json.dumps({'errors': form.errors}), content_type="application/json")

def addPic(request, dogId):
    message = 'failure'
    if request.method == 'POST': # If the form has been submitted...
        if request.FILES.has_key('file'):
            fileData = request.FILES['file']
            fileName = str(fileData)
            try:
                filePath = "%s.%s" % (dogId, fileName)
                absFilePath = os.path.join(
                        settings.DOG_MEDIA_ROOT, 
                        filePath
                    )
                with open(absFilePath, 'wb+') as dest:
                    for chunk in fileData.chunks():
                        dest.write(chunk)

                dog = Dog.objects.get(pk=int(dogId))

                dog.img = filePath
                dog.save()
                dog.processThumbnail()

                message = "success"
            except Exception, e:
                logger = logging.getLogger(__name__)
                logger.info(e)

    return HttpResponse(json.dumps({'status': message}), content_type="application/json")

def search(request):
    postData = json.loads(request.body)

    if request.method == 'POST' and postData.has_key('search'):
        searchTerm = postData['search']
        results = SearchQuerySet().filter(content__contains=searchTerm)
        results = DogSerializer([q.object for q in results])
        return HttpResponse(json.dumps({'status':'success', 'dogs':results.data}),
            content_type="application/json")

    return HttpResponse(json.dumps({'status': 'failure'}), content_type="application/json")








