from django import forms
from django.forms import ModelForm
from dogsafari.models import Dog

class DogForm(ModelForm):
    class Meta:
        model = Dog
        fields = ['name', 'owner']

    img = forms.CharField()
