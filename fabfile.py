import json
import os
from fabric.api import *
from fabric.contrib import files
from fabric.utils import abort, error

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

APP_ENV = json.load(open(os.path.join(PROJECT_PATH, 'config','fabric.json'), 'r'))

##########################################
# Connection Tasks
##########################################

# suppress logging exceptions
import logging
logging.raiseExceptions = False

@task
def vagrant(app='all', boot=True, destroy=False):
    if destroy: local('vagrant destroy')
    if boot: local('vagrant up --provider virtualbox')

    env.update(APP_ENV['vagrant'])

    # # use vagrant ssh key
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)

    print result
    env.key_filename = result.split()[1].strip('"')


##########################################
# Build tasks
# Build installs system dependencies for the projects.
##########################################

@task
def build():
    system_update()
    system_install('git') # for bower

    # python dev packages and pip dependencies
    system_install('python-software-properties python g++ make python-pip python-dev')
    
    # libjpeg for PIL
    system_install('libjpeg-dev libfreetype6 libfreetype6-dev zlib1g-dev')

    # virtualenv, installed globally.
    # whoosh is for searching
    sudopip(['virtualenv', 'whoosh'])

    node()

def sudopip(packages):
    sudo('sudo pip install %s' % ' '.join(packages), shell=False)

def system_update():
    sudo('apt-get -yqq update')

def system_install(*packages):
    sudo('apt-get -yqq install %s' % ' '.join(packages), shell=False)

def node():
    sudo('sudo add-apt-repository ppa:chris-lea/node.js', pty=False, quiet=True)
    system_update()

    system_install('nodejs')
    sudo('apt-get -yqq remove coffeescript')

    # `npm install` uses this folder sometimes, and will complain if it doesn't have proper access.
    sudo('mkdir -p ~/tmp')
    sudo('chmod 0777 ~/tmp')

    # install global requirements
    sudo('npm install -g grunt-cli coffee-script bower')



##########################################
# Clonecode/deploy task, and it's related subtasks
##########################################
# If this project was not just on vagrant, there would be logic here to clone out code.

# install dependencies for the project that are not global
@task
def deploy():
    with cd('~/app/dogsafari'):
        if not files.exists('env'):
            run('virtualenv env --no-site-packages')

        with prefix('source ./env/bin/activate'):
            pip_install_from_requirements_file()

    with cd('~/app/dogsafari/webapp'):
        run('npm install')
        run('bower install')
        run('grunt browserify')

def pip_install_from_requirements_file():
    run('pip install -r requirements.txt', shell=False)


##############################################
# restart_all task, and it's related subtasks
# Restart nginx
# restart circus or python
##############################################

@task
def syncdb():
    with cd('~/app/dogsafari'):
        with prefix('source ./env/bin/activate'):
            sudo('python manage.py syncdb')


@task
def runserver():
    with cd('~/app/dogsafari'):
        with prefix('source ./env/bin/activate'):
            sudo('python manage.py runserver 0.0.0.0:8000')

@task
def free8000():
    sudo('sudo kill `sudo lsof -t -i:8000`')


