
# Development version installation instructions

1. Have Virtualbox, Vagrant, python2.7, and fabric installed

2. Git clone me

3. Start up your virtualbox. Make sure that ports 2213 and 3070 are available. They're intentionally unusual inorder not to collide with commonly used ports.

		cd /path/to/rover_interview
		fab vagrant

4. Build and deploy

		fab vagrant build deploy
                fab vagrant syncdb

5. Start the django server

		fab vagrant runserver

6. Visit the application at [localhost:3070/dogsafari/](http://localhost:3070/dogsafari/)


----

# Rover Coding Project

The goal of this project to create a web interface for a database of dogs and their owners. You can use any language and frameworks you’d like to complete this project. While the project itself is simple, we're not just looking to see if you can complete the project. Please take your time and show us what you care about when you're writing software.


### Requirements

1. A user should be able to create a dog, entering the dog's name, their owner's name, and a photo of the dog

2. The newly created dog should display in a list of all dogs

3. Each dog in the list should show their name and a thumbnail photo

4. The thumbnail image should be resized to maximum dimensions of 150x150 and retain the aspect ratio of the original image

5. Images should be resized on the server

6. When the user clicks on a dog, display a lightbox that shows:
  - The dog's name
  - The dog's owner's name
  - A larger version of the dog's photo

7. Allow the user to search for a dog by dog name or dog owner name.


Complete this project and push your solution back to this repository. When you're finished up, email your Rover contact and let them know.
