from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rover_interview.views.home', name='home'),
    url(r'^dogsafari/', include('dogsafari.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('', 
    url(r'^bower_components/(?P<path>.*)$', 
        'django.views.static.serve', 
        {'document_root': settings.BOWER_ROOT}, 
        name="bower_components"),
    url(r'^build/(?P<path>.*)$', 'django.views.static.serve', 
        {'document_root': settings.BUILD_ROOT}, 
        name="build"),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', 
        {'document_root': settings.MEDIA_ROOT}, 
        name="media"),
    url(r'^favicon\.ico$', RedirectView.as_view(url='media/favicon.ico')),
)
